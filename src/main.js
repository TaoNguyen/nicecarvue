import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex'
import 'bootstrap'
import VueRouter from 'vue-router'
import AllIosIcon from 'vue-ionicons/dist/ionicons-ios.js'
 






Vue.config.productionTip = false
Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(AllIosIcon)

new Vue({
  render: h => h(App),
}).$mount('#app')
